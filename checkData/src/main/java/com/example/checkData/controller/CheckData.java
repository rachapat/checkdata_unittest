package com.example.checkData.controller;

import com.example.checkData.dto.CheckDataResponseDto;
import com.example.checkData.dto.DistrictDTO;
import com.example.checkData.dto.InputDataDTO;
import com.example.checkData.repository.ProvinceRepository;
import com.example.checkData.service.CheckDataService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.logging.Logger;

@Controller
@RestController
public class CheckData implements Serializable {

    @Autowired
    CheckDataService checkDataService;

    @PostMapping(value = "/checkData" , consumes = "application/json", produces = "application/json")
    public CheckDataResponseDto checkData(@RequestBody InputDataDTO input, HttpServletResponse response) throws IOException {

        return checkDataService.CheckData(input);
    }

}
