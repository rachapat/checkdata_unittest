package com.example.checkData.dto;

public class CheckDataResponseDto {


    private Boolean checkName=false;
    private Boolean checkTelephone=false;
    private Boolean checkProvince=false;
    private Boolean checkDistrict=false;

    public Boolean getCheckName() {
        return checkName;
    }

    public void setCheckName(Boolean checkName) {
        this.checkName = checkName;
    }

    public Boolean getCheckTelephone() {
        return checkTelephone;
    }

    public void setCheckTelephone(Boolean checkTelephone) {
        this.checkTelephone = checkTelephone;
    }

    public Boolean getCheckProvince() {
        return checkProvince;
    }

    public void setCheckProvince(Boolean checkProvince) {
        this.checkProvince = checkProvince;
    }

    public Boolean getCheckDistrict() {
        return checkDistrict;
    }

    public void setCheckDistrict(Boolean checkDistrict) {
        this.checkDistrict = checkDistrict;
    }
}
