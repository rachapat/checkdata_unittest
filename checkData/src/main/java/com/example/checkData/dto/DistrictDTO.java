package com.example.checkData.dto;

import java.util.List;

public class DistrictDTO {

    private String province;
    private List<String> districts;

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public List<String> getDistricts() {
        return districts;
    }

    public void setDistricts(List<String> districts) {
        this.districts = districts;
    }
}
