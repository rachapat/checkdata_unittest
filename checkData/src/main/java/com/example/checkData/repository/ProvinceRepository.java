package com.example.checkData.repository;

import com.example.checkData.entity.Province;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProvinceRepository extends JpaRepository<Province, Long> {
    Boolean existsByProvinceLike(String str);

}
