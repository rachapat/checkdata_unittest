package com.example.checkData.service;

import com.example.checkData.dto.CheckDataDto;
import com.example.checkData.dto.CheckDataResponseDto;
import com.example.checkData.dto.InputDataDTO;

import java.io.IOException;

public interface CheckDataService {
    public CheckDataResponseDto CheckData (InputDataDTO input) throws IOException;
    public CheckDataDto checkName(String name);
    public CheckDataDto checkTelephone(String telephone);
    public CheckDataDto checkProvince(String province);
    public CheckDataDto checkDistrict(String district) throws IOException;
}
