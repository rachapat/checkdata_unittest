package com.example.checkData.service.impements;

import com.example.checkData.dto.CheckDataDto;
import com.example.checkData.dto.CheckDataResponseDto;
import com.example.checkData.dto.InputDataDTO;
import com.example.checkData.repository.ProvinceRepository;
import com.example.checkData.service.CheckDataService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class CheckDataImpe implements CheckDataService {

    @Autowired
    ProvinceRepository provinceRepo;

    @Autowired
    ResourceLoader loader;

    @Value("${DistrictList.File}")
    private String DistrictListFile;

    public CheckDataResponseDto CheckData (InputDataDTO input) throws IOException {

        CheckDataResponseDto res = new CheckDataResponseDto();

        if(stringNoNull(input.getName()).matches("[a-zA-Z]*[\\s]{1}[a-zA-Z]*")||
                stringNoNull(input.getName()).matches("[\u0E00-\u0E7F]*[\\s]{1}[\u0E00-\u0E7F]*")
        ){
            res.setCheckName(true);
        }

//        else if(input.getFirstName().matches("[\\u0E00-\\u0E7F]+")){
//            res.setCheckFirstName(true);
//        }

        if(stringNoNull(input.getTelephone()).matches("[0]{1}[2-9]{1}[0-9]{7,8}")){
            res.setCheckTelephone(true);
        }

        if((stringNoNull(input.getProvince()).matches("[a-zA-Z\\s]+")
                ||stringNoNull(input.getProvince()).matches("[\u0E00-\u0E7F\\s]*"))
                && provinceRepo.existsByProvinceLike(stringNoNull(input.getProvince()))){
            res.setCheckProvince(true);
        }


        Resource resource = loader.getResource(DistrictListFile);
        InputStream inputStream = resource.getInputStream();
        try {
            ObjectMapper mapper = new ObjectMapper();
            byte[] bdata = FileCopyUtils.copyToByteArray(inputStream);
            String json = new String(bdata, StandardCharsets.UTF_8);

            List<String> districtList = new ArrayList<String>(Arrays.asList(json.split(",")));

            for (String s : districtList) {
                if((stringNoNull(input.getDistrict()).matches("[a-zA-Z\\s]*")
                        ||stringNoNull(input.getDistrict()).matches("[\u0E00-\u0E7F\\s]*"))
                        && stringNoNull(input.getDistrict()).equalsIgnoreCase(s.trim())){
                    res.setCheckDistrict(true);
//                    System.out.println(s+"\n");
                }

            }

        } catch (IOException e) {
            System.out.println("Err: "+e);
        }

        return res;
    }

    public CheckDataDto checkName(String name){
        CheckDataDto checkData = new CheckDataDto();
        checkData.setCheckData(name);

            if(
                    stringNoNull(name).matches("[A-Z]*[\\s]{1}[A-Z]*")||
                    stringNoNull(name).matches("[a-zA-Z]{1}[a-z]*[\\s]{1}[a-zA-Z]{1}[a-z]*")||
                    stringNoNull(name).matches("[\u0E00-\u0E7F]*[\\s]{1}[\u0E00-\u0E7F]*")
            ){

                checkData.setResult(true);
                checkData.setAnnotation("");
            }else{
                checkData.setResult(false);
                checkData.setAnnotation("Invalid name");
            }


        return checkData;
    }

    public CheckDataDto checkTelephone(String telephone){
        CheckDataDto checkData = new CheckDataDto();
        checkData.setCheckData(telephone);

        if(stringNoNull(telephone).matches("[0]{1}[2-9]{1}[0-9]{7,8}")){

            checkData.setResult(true);
            checkData.setAnnotation("");
        }else{
            checkData.setResult(false);
            checkData.setAnnotation("Invalid telephone number");
        }

        return checkData;
    }




    public CheckDataDto checkProvince(String province){
        CheckDataDto checkData = new CheckDataDto();
        checkData.setCheckData(province);

        if((stringNoNull(province).matches("[a-zA-Z\\s]+")
                ||stringNoNull(province).matches("[\u0E00-\u0E7F\\s]*"))
                &&!stringNoNull(province).isEmpty()
        ){

            if(provinceRepo.existsByProvinceLike(stringNoNull(province))){
                checkData.setResult(true);
                checkData.setAnnotation("");
            }else{
                checkData.setResult(false);
                checkData.setAnnotation("Not match in database");
            }

        }else{
            checkData.setResult(false);
            checkData.setAnnotation("Invalid Province");
        }

        return checkData;
    }


    public CheckDataDto checkDistrict(String district) throws IOException {
        CheckDataDto checkData = new CheckDataDto();
        checkData.setCheckData(district);

        if((stringNoNull(district).matches("[a-zA-Z\\s]+")
                ||stringNoNull(district).matches("[\u0E00-\u0E7F\\s]*"))
                &&!stringNoNull(district).isEmpty()
        ){

            Resource resource = loader.getResource(DistrictListFile);
            InputStream inputStream = resource.getInputStream();
            try {
                ObjectMapper mapper = new ObjectMapper();
                byte[] bdata = FileCopyUtils.copyToByteArray(inputStream);
                String json = new String(bdata, StandardCharsets.UTF_8);

                List<String> districtList = new ArrayList<String>(Arrays.asList(json.split(",")));

                for (String s : districtList) {
                    if(stringNoNull(district).equalsIgnoreCase(s.trim())){
                        checkData.setResult(true);
                        checkData.setAnnotation("");
                        break;
                    }

                }

                if(!checkData.getResult()){
                    checkData.setResult(false);
                    checkData.setAnnotation("Not match in list");
                }

            } catch (IOException e) {
                System.out.println("Err: "+e);
            }

        }else{
            checkData.setResult(false);
            checkData.setAnnotation("Invalid District");
        }

        return checkData;
    }


    public String stringNoNull(String str){
        if(str == null || str.isEmpty() || str.isBlank()){
            return "";
        }else{
            return str.trim();
        }

    }

}
