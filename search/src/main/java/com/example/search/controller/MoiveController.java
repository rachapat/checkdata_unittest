package com.example.search.controller;

import com.example.search.cacde.Cacde;
import com.example.search.entity.Movie;
import com.example.search.repository.MovieRepository;
import com.example.search.service.SearchMovieService;
import com.example.search.service.implement.SearchMovieImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Controller
@RestController
public class MoiveController {

    @Autowired
    SearchMovieService searchMovieService;

    @PostMapping(value = "/movie/full-search/{wordSearch}")
    public List<Movie> fullSearchMovie(@PathVariable String wordSearch) throws IOException {
        List<Movie> movieList = searchMovieService.fullSearchMovie(wordSearch);
        return movieList;
    }

    @PostMapping(value = "/movie/partial-search/{wordSearch}")
    public List<Movie> partialSearchMovie(@PathVariable String wordSearch) throws IOException {
        List<Movie> movieList = searchMovieService.partialSearchMovie(wordSearch);
        return movieList;
    }

    @PostMapping(value = "/movie/index-search/{wordSearch}")
    public List<Movie> wordSearch(@PathVariable String wordSearch) throws IOException {
        List<Movie> movieList = searchMovieService.invertedIndexSearchMovie(wordSearch);
        return movieList;
    }


}
