package com.example.search;

import com.example.search.cacde.Cacde;
import com.example.search.entity.Movie;
import com.example.search.repository.MovieRepository;
import com.example.search.service.InsertMovieDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@SpringBootApplication
public class SearchApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(SearchApplication.class, args);
    }

    @Autowired
    InsertMovieDataService insertMovieDataService;

    @Override
    public void run(String... args) throws Exception {
//        insertMovieDataService.insertMovie();
        insertMovieDataService.invertedIndexList();
    }

    

}
