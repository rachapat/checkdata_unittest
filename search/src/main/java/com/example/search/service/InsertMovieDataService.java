package com.example.search.service;

import java.io.IOException;

public interface InsertMovieDataService {
    public void insertMovie() throws IOException;
    public void invertedIndexList();
}
