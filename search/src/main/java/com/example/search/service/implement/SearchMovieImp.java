package com.example.search.service.implement;

import com.example.search.cacde.Cacde;
import com.example.search.entity.Movie;
import com.example.search.service.SearchMovieService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class SearchMovieImp extends CommonImp implements SearchMovieService {

    public List<Movie> fullSearchMovie(String wordSearch){
        List<Movie> movieList = movieRepo.findAllByMovieNameLike(wordSearch);
        return movieList;
    }

    public List<Movie> partialSearchMovie(String wordSearch){
        List<Movie> movieList = movieRepo.findAllByMovieNameIgnoreCaseContaining(wordSearch);
        return movieList;
    }

    public List<Movie> invertedIndexSearchMovie(String wordSearch){
        List<String> wordList = removeDuplicates(Arrays.stream(wordSearch.split(" ")).map(String::trim).collect(Collectors.toList()));
        List<Long> idmovie = new ArrayList<>();
        for(String s : wordList){
            if(Cacde.invertedIndexList.get(s)!=null){
                if(idmovie.isEmpty()){
                    idmovie = Stream.concat(idmovie.stream(), Cacde.invertedIndexList.get(s).stream()).collect(Collectors.toList());
                }else{
                    idmovie = intersection(idmovie,Cacde.invertedIndexList.get(s));
                }
            }
        }
        if(idmovie.size()>0){
            HashSet<Long> idList= new HashSet(idmovie);
            return movieRepo.findAllByIdIn(idList);
        }
        return null;
    }
}
