package com.example.search.service;

import com.example.search.entity.Movie;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Service
public interface SearchMovieService {
    public List<Movie> fullSearchMovie(String wordSearch);
    public List<Movie> partialSearchMovie(String wordSearch);
    public List<Movie> invertedIndexSearchMovie(String wordSearch);
}
