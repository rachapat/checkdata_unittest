package com.example.search.repository;

import com.example.search.entity.Cast;
import com.example.search.entity.Genres;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GenresRepository extends JpaRepository<Genres, Long> {
    boolean existsByGenresNameLike(String genres);
    Genres findByGenresNameLike(String genres);
}
