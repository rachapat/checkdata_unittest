package com.example.search.repository;

import com.example.search.entity.Cast;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CastRepository extends JpaRepository<Cast, Long> {
    boolean existsByCastNameLike(String Cast);
    Cast findByCastNameLike(String Cast);
}
