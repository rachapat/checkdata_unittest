package com.example.search;

import com.example.search.dto.MovieDataDTO;
import com.example.search.entity.Cast;
import com.example.search.entity.Genres;
import com.example.search.entity.Movie;
import com.example.search.repository.CastRepository;
import com.example.search.repository.GenresRepository;
import com.example.search.repository.MovieRepository;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.util.FileCopyUtils;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.*;

@SpringBootTest
class SearchApplicationTests {


    @Autowired
    ResourceLoader loader;

    @Autowired
    MovieRepository movieRepo;

    @Autowired
    CastRepository castRepo;

    @Autowired
    GenresRepository genresRepo;


    @Test
    void insertMovieOne() throws IOException {
        String url = "https://raw.githubusercontent.com/prust/wikipedia-movie-data/master/movies.json";
        Resource resource = loader.getResource(url);
        InputStream inputStream = resource.getInputStream();
        ObjectMapper mapper = new ObjectMapper();
        byte[] bdata = FileCopyUtils.copyToByteArray(inputStream);
        String json = new String(bdata, StandardCharsets.UTF_8);
//        System.out.println(json);


        List<MovieDataDTO> movieList = mapper.readValue(json, new TypeReference<List<MovieDataDTO>>() {});
        for(MovieDataDTO data : movieList){

            if(data.getGenres().size()>0&&data.getCast().size()>0){
                Movie movie = new Movie();
                List<Cast> casts = new ArrayList<>();
                List<Genres> genres = new ArrayList<>();

                movie.setMovieName(data.getTitle());
                movie.setYear(Long.parseLong(data.getYear()));

                movieRepo.saveAndFlush(movie);

                if(data.getCast().size()>0){
                    for(String s : data.getCast()){
                        if(!castRepo.existsByCastNameLike(s)){
                            Cast cast = new Cast();
                            cast.setCastName(s);
                            castRepo.saveAndFlush(cast);
                        }
                        casts.add(castRepo.findByCastNameLike(s));
                    }
                    movie.setCastsList(casts);
                }

                if(data.getGenres().size()>0){
                    for(String s : data.getGenres()){
                        if(!genresRepo.existsByGenresNameLike(s)){
                            Genres genres1 = new Genres();
                            genres1.setGenresName(s);
                            genresRepo.saveAndFlush(genres1);
                        }
                        genres.add(genresRepo.findByGenresNameLike(s));
                    }
                    movie.setGenresList(genres);
                }

                movieRepo.saveAndFlush(movie);

                break;
            }
        }

    }

    @Test
    void insertMovieAll() throws IOException {
        String url = "https://raw.githubusercontent.com/prust/wikipedia-movie-data/master/movies.json";
        Resource resource = loader.getResource(url);
        InputStream inputStream = resource.getInputStream();
        ObjectMapper mapper = new ObjectMapper();
        byte[] bdata = FileCopyUtils.copyToByteArray(inputStream);
        String json = new String(bdata, StandardCharsets.UTF_8);
//        System.out.println(json);


        List<MovieDataDTO> movieList = mapper.readValue(json, new TypeReference<List<MovieDataDTO>>() {});
        for(MovieDataDTO data : movieList){

            if(!movieRepo.existsByMovieNameLikeAndYear(data.getTitle(),Long.parseLong(data.getYear()))){
                Movie movie = new Movie();
                List<Cast> casts = new ArrayList<>();
                List<Genres> genres = new ArrayList<>();

                movie.setMovieName(data.getTitle());
                movie.setYear(Long.parseLong(data.getYear()));

                movieRepo.saveAndFlush(movie);

                if(data.getCast().size()>0){
                    for(String s : data.getCast()){
                        if(!castRepo.existsByCastNameLike(s)){
                            Cast cast = new Cast();
                            cast.setCastName(s);
                            castRepo.saveAndFlush(cast);
                        }
                        casts.add(castRepo.findByCastNameLike(s));
                    }
                    movie.setCastsList(casts);
                }

                if(data.getGenres().size()>0){
                    for(String s : data.getGenres()){
                        if(!genresRepo.existsByGenresNameLike(s)){
                            Genres genres1 = new Genres();
                            genres1.setGenresName(s);
                            genresRepo.saveAndFlush(genres1);
                        }
                        genres.add(genresRepo.findByGenresNameLike(s));
                    }
                    movie.setGenresList(genres);
                }

                movieRepo.saveAndFlush(movie);
            }

        }


    }

}
