package com.example.unitTest;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class UnitTest extends UnitTestApplicationTests {

    @Test
    void unitTest_CheckName() {

        //---------------------------- Test Check Name -------------------------------

        assertEquals(true, checkDataService.checkName("Rachapat Dangsawat").getResult());
        assertEquals("", checkDataService.checkName("Rachapat Dangsawat").getAnnotation());
        assertEquals(true, checkDataService.checkName("รชต์พัชร แดงสวัสดิ์").getResult());
        assertEquals("", checkDataService.checkName("รชต์พัชร แดงสวัสดิ์").getAnnotation());

        assertEquals(true, checkDataService.checkName("rachapat dangsawat").getResult());
        assertEquals(true, checkDataService.checkName("RACHAPAT DANGSAWAT").getResult());

        assertEquals(false, checkDataService.checkName("Rachapat แดงสวัสดิ์").getResult());
        assertEquals("Invalid name", checkDataService.checkName("Rachapat แดงสวัสดิ์").getAnnotation());

        assertEquals(false, checkDataService.checkName("รชต์พัชร$ แดงสวัสดิ์").getResult());
        assertEquals("Invalid name", checkDataService.checkName("รชต์พัชร$ แดงสวัสดิ์").getAnnotation());
        assertEquals(false, checkDataService.checkName("r@chapat !@#$%^&").getResult());
        assertEquals("Invalid name", checkDataService.checkName("r@chapat !@#$%^&").getAnnotation());
        assertEquals(false, checkDataService.checkName("rachapat").getResult());
        assertEquals("Invalid name", checkDataService.checkName("rachapat").getAnnotation());

        assertEquals(false, checkDataService.checkName("RaChApAt DaNgSaWat").getResult());
        assertEquals(false, checkDataService.checkName("rachAPAT Dangsawta").getResult());
        assertEquals(false, checkDataService.checkName("rachapat DANGSAWAT").getResult());
        assertEquals(false, checkDataService.checkName("RACHAPAT dangsawat").getResult());
        assertEquals(false, checkDataService.checkName("RACHAPAT_DANGSAWAT").getResult());
        assertEquals(false, checkDataService.checkName("").getResult());
        assertEquals(false, checkDataService.checkName("  ").getResult());
        assertEquals(false, checkDataService.checkName(null).getResult());

    }

    @Test
    void unitTest_CheckTelephone(){

        //---------------------------- Test Check Telephone -------------------------------
        assertEquals(true, checkDataService.checkTelephone("0937631750").getResult());
        assertEquals("", checkDataService.checkTelephone("0937631750").getAnnotation());
        assertEquals(true, checkDataService.checkTelephone("077551675").getResult());
        assertEquals("", checkDataService.checkTelephone("077551675").getAnnotation());
        assertEquals(true, checkDataService.checkTelephone("026911958").getResult());
        assertEquals("", checkDataService.checkTelephone("026911958").getAnnotation());

        assertEquals(false, checkDataService.checkTelephone("016911958").getResult());
        assertEquals("Invalid telephone number", checkDataService.checkTelephone("016911958").getAnnotation());
        assertEquals(false, checkDataService.checkTelephone("02691195").getResult());
        assertEquals("Invalid telephone number", checkDataService.checkTelephone("02691195").getAnnotation());
        assertEquals(false, checkDataService.checkTelephone("o937631750").getResult());
        assertEquals("Invalid telephone number", checkDataService.checkTelephone("o937631750").getAnnotation());

        assertEquals(false, checkDataService.checkTelephone("o๙๓๗๖๓๑๗๕o").getResult());
        assertEquals(false, checkDataService.checkTelephone("cvbnhgfdhj").getResult());
        assertEquals(false, checkDataService.checkTelephone("").getResult());
        assertEquals(false, checkDataService.checkTelephone("  ").getResult());
        assertEquals(false, checkDataService.checkTelephone(null).getResult());


    }

    @Test
    void unitTest_CheckProvince() {
        assertEquals(true, checkDataService.checkProvince("ชุมพร").getResult());
        assertEquals("", checkDataService.checkProvince("ชุมพร").getAnnotation());
        assertEquals(true, checkDataService.checkProvince("New York").getResult());
        assertEquals("", checkDataService.checkProvince("New York").getAnnotation());
        assertEquals(true, checkDataService.checkProvince("new york").getResult());
        assertEquals("", checkDataService.checkProvince("new york").getAnnotation());
        assertEquals(true, checkDataService.checkProvince("NEW YORK").getResult());
        assertEquals("", checkDataService.checkProvince("NEW YORK").getAnnotation());

        assertEquals(false, checkDataService.checkProvince("NEW YEAR").getResult());
        assertEquals("Not match in database", checkDataService.checkProvince("NEW YEAR").getAnnotation());
        assertEquals(false, checkDataService.checkProvince("NEWYORK").getResult());
        assertEquals("Not match in database", checkDataService.checkProvince("NEWYORK").getAnnotation());

        assertEquals(false, checkDataService.checkProvince("New Y0rk").getResult());
        assertEquals("Invalid Province", checkDataService.checkProvince("New Y0rk").getAnnotation());
        assertEquals(false, checkDataService.checkProvince("new1 york").getResult());
        assertEquals("Invalid Province", checkDataService.checkProvince("new1 york").getAnnotation());
        assertEquals(false, checkDataService.checkProvince("12312").getResult());
        assertEquals("Invalid Province", checkDataService.checkProvince("12312").getAnnotation());
        assertEquals(false, checkDataService.checkProvince("W#$$").getResult());
        assertEquals("Invalid Province", checkDataService.checkProvince("W#$$").getAnnotation());

        assertEquals(false, checkDataService.checkProvince("").getResult());
        assertEquals("Invalid Province", checkDataService.checkProvince("").getAnnotation());
        assertEquals(false, checkDataService.checkProvince(" ").getResult());
        assertEquals("Invalid Province", checkDataService.checkProvince(" ").getAnnotation());
        assertEquals(false, checkDataService.checkProvince(null).getResult());
        assertEquals("Invalid Province", checkDataService.checkProvince(null).getAnnotation());
    }

    @Test
    void unitTest_CheckDistrict() throws IOException {
        assertEquals(true, checkDataService.checkDistrict("Khet Don Mueang").getResult());
        assertEquals("", checkDataService.checkDistrict("Khet Don Mueang").getAnnotation());
        assertEquals(true, checkDataService.checkDistrict("Khet Don Mueang  ").getResult());
        assertEquals("", checkDataService.checkDistrict("Khet Don Mueang   ").getAnnotation());
        assertEquals(true, checkDataService.checkDistrict("khet don mueang").getResult());
        assertEquals("", checkDataService.checkDistrict("khet don mueang").getAnnotation());
        assertEquals(true, checkDataService.checkDistrict("KHET DON MUEANG").getResult());
        assertEquals("", checkDataService.checkDistrict("KHET DON MUEANG").getAnnotation());
        assertEquals(true, checkDataService.checkDistrict("Khet Khlong San").getResult());
        assertEquals(true, checkDataService.checkDistrict("เขตดินแดง").getResult());
        //Khet Khlong San
        assertEquals(false, checkDataService.checkDistrict("KhetKhlongSan").getResult());
        assertEquals("Not match in list", checkDataService.checkDistrict("KhetKhlongSan").getAnnotation());
        assertEquals(false, checkDataService.checkDistrict("ดินแดง").getResult());
        assertEquals("Not match in list", checkDataService.checkDistrict("ดินแดง").getAnnotation());

        assertEquals(false, checkDataService.checkDistrict("เขตดินแดงA").getResult());
        assertEquals("Invalid District", checkDataService.checkDistrict("เขตดินแดงA").getAnnotation());
        assertEquals(false, checkDataService.checkDistrict("เขตดินแดง15").getResult());
        assertEquals("Invalid District", checkDataService.checkDistrict("เขตดินแดง15").getAnnotation());
        assertEquals(false, checkDataService.checkDistrict("Khet_Khlong_San").getResult());
        assertEquals("Invalid District", checkDataService.checkDistrict("Khet_Khlong_San").getAnnotation());
        assertEquals(false, checkDataService.checkDistrict("Khet$Khlong&San").getResult());
        assertEquals("Invalid District", checkDataService.checkDistrict("Khet$Khlong&San").getAnnotation());
        assertEquals(false, checkDataService.checkDistrict("$%").getResult());
        assertEquals("Invalid District", checkDataService.checkDistrict("$%").getAnnotation());
        assertEquals(false, checkDataService.checkDistrict("ASDา").getResult());
        assertEquals("Invalid District", checkDataService.checkDistrict("ASDา").getAnnotation());

        assertEquals(false, checkDataService.checkDistrict("").getResult());
        assertEquals("Invalid District", checkDataService.checkDistrict("").getAnnotation());
        assertEquals(false, checkDataService.checkDistrict(" ").getResult());
        assertEquals("Invalid District", checkDataService.checkDistrict(" ").getAnnotation());
        assertEquals(false, checkDataService.checkDistrict(null).getResult());
        assertEquals("Invalid District", checkDataService.checkDistrict(null).getAnnotation());
    }
}
