package com.example.unitTest;

import com.example.checkData.dto.CheckDataResponseDto;
import com.example.checkData.dto.InputDataDTO;
import com.example.checkData.service.CheckDataService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class UnitTestApplicationTests {


	@Autowired
	CheckDataService checkDataService;

	@Test
	void unitTest01() throws IOException {
		ObjectMapper mapper = new ObjectMapper();
		String checkName = mapper.writeValueAsString(checkDataService.checkName("rachapat dangsawat"));
		String checkTele = mapper.writeValueAsString(checkDataService.checkTelephone("0937631750"));
		String checkProv = mapper.writeValueAsString(checkDataService.checkProvince("yYc"));
		String checkDist = mapper.writeValueAsString(checkDataService.checkDistrict("เขตดินแดง"));
//		System.out.println("\n");
//		System.out.println(checkName);
//		System.out.println(checkTele);
//		System.out.println(checkProv);
//		System.out.println(checkDist);
//
//		checkName = mapper.writeValueAsString(checkDataService.checkName("rachapatdangsawat"));
//		checkTele = mapper.writeValueAsString(checkDataService.checkTelephone("0137631750"));
//		checkProv = mapper.writeValueAsString(checkDataService.checkProvince("yY"));
//		checkDist = mapper.writeValueAsString(checkDataService.checkDistrict("ดินแดง"));
//		System.out.println("\n");
//		System.out.println(checkName);
//		System.out.println(checkTele);
//		System.out.println(checkProv);
//		System.out.println(checkDist);

		checkProv = mapper.writeValueAsString(checkDataService.checkProvince("yY1"));
		checkDist = mapper.writeValueAsString(checkDataService.checkDistrict("ดินแดง1"));
		System.out.println("\n");
		System.out.println(checkProv);
		System.out.println(checkDist);

		checkName = mapper.writeValueAsString(checkDataService.checkName("รชต์พัชร แดงสวัสดิ์"));
		System.out.println("\n");
		System.out.println(checkName);

	}

	@Test
	void unitTest02() throws IOException {
		String name     = "GG EZ";
		String tel      = "0937631750"; // 10 number
		String province = "pr";
		String district = "Khet Don mueang";
		InputDataDTO input = setInputData(name,tel,province,district);
		CheckDataResponseDto res = checkDataService.CheckData(input);
		assertEquals(true, res.getCheckName());
		assertEquals(true, res.getCheckTelephone());
		assertEquals(true, res.getCheckProvince());
		assertEquals(true, res.getCheckDistrict());

		name     = "รชต์พัชร แดงสวัสดิ์";// name thai
		input = setInputData(name,tel,province,district);
		res = checkDataService.CheckData(input);
		assertEquals(true, res.getCheckName());


		tel      = "077551675"; // 9 number
		input = setInputData(name,tel,province,district);
		res = checkDataService.CheckData(input);
		assertEquals(true, res.getCheckTelephone());


		province = "Pr"; //this in DB but upper all word
		input = setInputData(name,tel,province,district);
		res = checkDataService.CheckData(input);
		assertEquals(true, res.getCheckProvince());

		province = "pr"; //this in DB but upper all word
		input = setInputData(name,tel,province,district);
		res = checkDataService.CheckData(input);
		assertEquals(true, res.getCheckProvince());

		province = "New York"; //this in DB but upper all word
		input = setInputData(name,tel,province,district);
		res = checkDataService.CheckData(input);
		assertEquals(true, res.getCheckProvince());

		province = "new york"; //this in DB but upper all word
		input = setInputData(name,tel,province,district);
		res = checkDataService.CheckData(input);
		assertEquals(true, res.getCheckProvince());

		province = "yyc"; //this in DB but upper some word
		district = "khet don mueang";
		input = setInputData(name,tel,province,district);
		res = checkDataService.CheckData(input);
		assertEquals(true, res.getCheckProvince());
		assertEquals(true, res.getCheckDistrict());

		district = "KHET DON MUEANG";
		input = setInputData(name,tel,province,district);
		res = checkDataService.CheckData(input);
		assertEquals(true, res.getCheckDistrict());


		district = "Khet Bangkok Noi";
		input = setInputData(name,tel,province,district);
		res = checkDataService.CheckData(input);
		assertEquals(true, res.getCheckDistrict());


		district = "KHET Bangkok NOI";
		input = setInputData(name,tel,province,district);
		res = checkDataService.CheckData(input);
		assertEquals(true, res.getCheckDistrict());

		province = "ชุมพร"; //this in DB but upper all word
		input = setInputData(name,tel,province,district);
		res = checkDataService.CheckData(input);
		assertEquals(true, res.getCheckProvince());

		district = "เขตดินแดง";
		input = setInputData(name,tel,province,district);
		res = checkDataService.CheckData(input);
		assertEquals(true, res.getCheckDistrict());


		//-----------------------------------------------------------------


		name     = ""; //no data
		tel      = ""; //no data
		province = ""; //no data
		district = ""; //no data
		input = setInputData(name,tel,province,district);
		res = checkDataService.CheckData(input);
		assertEquals(false, res.getCheckName());
		assertEquals(false, res.getCheckTelephone());
		assertEquals(false, res.getCheckProvince());
		assertEquals(false, res.getCheckDistrict());


		name     = "    "; //space
		tel      = "    "; //space
		province = "    "; //space
		district = "    "; //space
		input = setInputData(name,tel,province,district);
		res = checkDataService.CheckData(input);
		assertEquals(false, res.getCheckName());
		assertEquals(false, res.getCheckTelephone());
		assertEquals(false, res.getCheckProvince());
		assertEquals(false, res.getCheckDistrict());


		name     = null; //null
		tel      = null; //null
		province = null; //null
		district = null; //null
		input = setInputData(name,tel,province,district);
		res = checkDataService.CheckData(input);
		assertEquals(false, res.getCheckName());
		assertEquals(false, res.getCheckTelephone());
		assertEquals(false, res.getCheckProvince());
		assertEquals(false, res.getCheckDistrict());


		name     = "GGEZ"; //no last name
		tel      = "01376317"; // 8 number
		province = "prr"; //no this in DB
		district = "Khet "; //no this in file
		input = setInputData(name,tel,province,district);
		res = checkDataService.CheckData(input);
		assertEquals(false, res.getCheckName());
		assertEquals(false, res.getCheckTelephone());
		assertEquals(false, res.getCheckProvince());
		assertEquals(false, res.getCheckDistrict());


		name     = "!@#$$% !@#$%%"; //no last name
		tel      = "09^#!$&(^$"; // 8 number
		input = setInputData(name,tel,province,district);
		res = checkDataService.CheckData(input);
		assertEquals(false, res.getCheckProvince());
		assertEquals(false, res.getCheckDistrict());



		name     = "รชต์พัชรแดงสวัสดิ์";// name thai
		input = setInputData(name,tel,province,district);
		res = checkDataService.CheckData(input);
		assertEquals(false, res.getCheckName());


		name     = " GGEZ"; //last name only
		tel      = "11376317"; // not 0 start and 7 number
		district = "Khet12"; //no this in file and number inside
		input = setInputData(name,tel,province,district);
		res = checkDataService.CheckData(input);
		assertEquals(false, res.getCheckName());
		assertEquals(false, res.getCheckTelephone());
		assertEquals(false, res.getCheckDistrict());


		name     = "GGEZ123 sd123"; //number inside name
		tel      = "013763171"; // 01 and 7 number
		input = setInputData(name,tel,province,district);
		res = checkDataService.CheckData(input);
		assertEquals(false, res.getCheckName());
		assertEquals(false, res.getCheckTelephone());


		name     = "1234 1234"; //number only
		tel      = "093763"; // 6 number
		input = setInputData(name,tel,province,district);
		res = checkDataService.CheckData(input);
		assertEquals(false, res.getCheckName());
		assertEquals(false, res.getCheckTelephone());

		province = "ชุ ม พ ร"; //this in DB but upper all word
		input = setInputData(name,tel,province,district);
		res = checkDataService.CheckData(input);
		assertEquals(false, res.getCheckProvince());


	}



	public InputDataDTO setInputData(String name,String telephone,String province,String district){
		InputDataDTO input = new InputDataDTO();
		input.setName(name);
		input.setTelephone(telephone);
		input.setProvince(province);
		input.setDistrict(district);
		return input;
	}

}
