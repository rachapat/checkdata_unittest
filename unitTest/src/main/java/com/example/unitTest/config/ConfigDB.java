//package com.example.unitTest.config;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Qualifier;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.boot.autoconfigure.domain.EntityScan;
//import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
//import org.springframework.boot.context.properties.ConfigurationProperties;
//import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.context.annotation.Primary;
//import org.springframework.core.env.Environment;
//import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
//import org.springframework.jdbc.datasource.DriverManagerDataSource;
//import org.springframework.jdbc.datasource.lookup.JndiDataSourceLookup;
//import org.springframework.orm.jpa.JpaTransactionManager;
//import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
//import org.springframework.transaction.PlatformTransactionManager;
//import org.springframework.transaction.annotation.EnableTransactionManagement;
//
//import javax.persistence.EntityManagerFactory;
//import javax.sql.DataSource;
//import java.util.HashMap;
//import java.util.Map;
//
//@Configuration
//@EnableTransactionManagement
//@EnableJpaRepositories(basePackages = "com.example.checkData.entity", entityManagerFactoryRef = "merchantserviceEntityManagerFactory", transactionManagerRef = "merchantserviceTransactionManager")
//@EntityScan(basePackages = "com.example")
//public class ConfigDB {
//    @Autowired
//    private Environment env;
//
//    @Value("${jndi.datasource}")
//    private boolean useDatasource;
//
////    @Primary
//    @Bean(name = "merchantserviceEntityManagerFactory")
//    public LocalContainerEntityManagerFactoryBean merchantserviceEntityManagerFactory(EntityManagerFactoryBuilder builder,
//                                                                                      @Qualifier("merchantserviceDataSource") DataSource dataSource) {
//        Map<String, Object> properties = new HashMap<String, Object>();
////        properties.put("hibernate.hbm2ddl.auto", env.getProperty("hibernate.hbm2ddl.auto"));
////        properties.put("hibernate.dialect", env.getProperty("merchantservice.datasource.jpa.properties.hibernate.dialect"));
////        properties.put("hibernate.show_sql", env.getProperty("hibernate.show_sql"));
//        properties.put("hibernate.physical_naming_strategy",env.getProperty("merchant.properties.hibernate.physical_naming_strategy"));
//        return builder.dataSource(dataSource).packages("com.example.checkData.entity")
//                .properties(properties)
//                .persistenceUnit("merchantservice").build();
//    }
////    @Primary
//    @Bean(name = "merchantserviceDataSource")
//    public DataSource dataSource() {
//
//            DriverManagerDataSource dataSource = new DriverManagerDataSource();
//            dataSource.setDriverClassName(env.getProperty("merchantservice.datasource.driverClassName"));
//            dataSource.setUrl(env.getProperty("merchantservice.datasource.url"));
//            dataSource.setUsername(env.getProperty("merchantservice.datasource.username"));
//            dataSource.setPassword(env.getProperty("merchantservice.datasource.password"));
//            return dataSource;
//
//    }
//
//    @Bean
//    @ConfigurationProperties("merchantservice.datasource")
//    public DataSourceProperties dataSourceProperties() {
//        return new DataSourceProperties();
//    }
//
//    @Bean(name = "merchantserviceTransactionManager")
//    public PlatformTransactionManager merchantserviceTransactionManager(
//            @Qualifier("merchantserviceEntityManagerFactory") EntityManagerFactory merchantserviceTransactionManager) {
//        return new JpaTransactionManager(merchantserviceTransactionManager);
//    }
//
//}
