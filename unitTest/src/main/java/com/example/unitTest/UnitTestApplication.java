package com.example.unitTest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan(basePackages = {"com.example.unitTest","com.example.checkData"})
@EnableJpaRepositories(basePackages = "com.example.checkData.repository")
public class UnitTestApplication {



	public static void main(String[] args) {

		SpringApplication.run(UnitTestApplication.class, args);

	}

}
